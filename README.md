ics-ans-role-postgres
===================

Ansible role to install postgres.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-postgres
```

License
-------

BSD 2-clause
